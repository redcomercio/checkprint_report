# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name' : 'Check Print',
    'version' : '1.1',
    'summary': 'Check Print',
    'sequence': 30,
    'description': """
Check Print
==============
This module is used to print the check Report.
    """,
    'category': 'Accounting',
    'website': 'https://www.justcodify.com/',
    'author' : 'Just Codify', 
    'images' : [],
    'depends' : ['account_check_printing'],
    'data': [
             'views/print_check_template.xml',
             'views/print_check_report.xml'
    ],
    'installable': True,
    'auto_install': False,
}