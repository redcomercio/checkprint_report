# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import models,fields,_,api
from num2words import num2words
from itertools import repeat

class AccountPaymentReport(models.AbstractModel):
    
    _name = "report.checkprint_report.report_print_check_template"
    
    @api.model
    def render_html(self, docids, data=None):      
        data = data if data is not None else {}
        docargs = {
                'doc_ids': data.get('ids', data.get('active_ids')),
                'data':{
                    'payment' : data['payments'] or []              
                    }
                }
        return self.env['report'].render('checkprint_report.report_print_check_template',docargs)
    
class PrintPreNumberedChecksinherit(models.TransientModel):
    _inherit = 'print.prenumbered.checks'
    
    @api.multi
    def print_checks(self):
        check_number = self.next_check_number
        payments = self.env['account.payment'].browse(self.env.context['payment_ids'])
        payments.filtered(lambda r: r.state == 'draft').post()
        payments.filtered(lambda r: r.state != 'sent').write({'state': 'sent'})
        payment_data = []
        
        for payment in payments:
            data = {}
            lines = []
            payment.check_number = check_number
            check_number += 1
            data['check'] = payment.check_number,
            data['partner'] = payment.partner_id.name,
            data['partner_vat'] = ('{0:,}'.format(int(payment.partner_id.vat[2:-1]))+'-'+payment.partner_id.vat[-1]).replace(",",".")
            data['date'] = payment.payment_date,
            data['amount'] = payment.amount,
            data['lines'] = []
            debit_sum = 0
            credit_sum = 0
            i = 0
            while i<18:
                line = {}
                if len(payment.move_line_ids) > i:
                    journal = payment.move_line_ids[i]
                    line['account'] = journal.account_id.name
                    line['label'] = journal.name
                    line['debit'] = journal.debit 
                    debit_sum += journal.debit
                    line['credit'] = journal.credit
                    credit_sum += journal.credit
                else:
                    line['account'] = "-"
                    line['label'] = "-"
                    line['debit'] = "-"
                    line['credit'] = "-"
                data['lines'].append(line)
                i = i+1
            data['debit_sum'] = debit_sum
            data['credit_sum'] = credit_sum
            data['amount_in_word'] = num2words(debit_sum, lang='es')
            payment_data.append(data)
        datas  = {
                  'ids': self.env.context.get('active_ids', []),
                  'payments' : payment_data,
                  }
        return self.env['report'].get_action([],'checkprint_report.report_print_check_template',data=datas)
    
